#pragma once
#include "Repository.h"

class UndoMove {
public:
	ChessPiece* p1;
	ChessPiece* p2;
	int idx;
public:
	UndoMove(ChessPiece* pies1, ChessPiece* pies2, int idx) :idx{ idx } {
		p1 = createPieceCopy(pies1);
		if (pies2 == nullptr) {
			p2 = nullptr;
		}
		else {
			p2 = createPieceCopy(pies2);
		}
	}
};

class MoveDTO {
private:
	ChessPiece* p;
	double eval;
public:
	MoveDTO(ChessPiece* p, double ev) :p{ p }, eval{ ev }{}
	void setEval(const int& ev) {
		this->eval = ev;
	}
	ChessPiece*& getPiece() {
		return this->p;
	}
	int getEval() {
		return this->eval;
	}
};
class Service {
private:
	Repository& repo;
	int whosMovin;
	vector<UndoMove> undoList;
public:
	Service(Repository& repo) :repo{ repo }, whosMovin{ 0 } {}
	vector<Square>& getBoard();
	bool updateBoard(ChessPiece*& piece, const int& idx);
	bool isInCheck(ChessPiece* kg);
	ChessPiece* getWhiteKing()const {
		return this->repo.getWhiteKing();
	}
	ChessPiece* getBlackKing()const {
		return this->repo.getBlackKing();
	}
	vector<pair<string, string>>& getMoves() {
		return this->repo.getMoves();
	}
	void setOnBoard(ChessPiece* piece, int idx) {
		this->repo.setOnBoard(piece, idx);
	}
	int getWhosMovin() {
		return this->whosMovin;
	}
	void clearBoard();
	void changePlayer();
	void doUndo();
	void miniMax(int depth, int alfa, int beta, int whosMovin);
	MoveDTO recMiniMax(Service& srv, ChessPiece* p, int depth, int alfa, int beta, int whosMovin);
	double evalPos(vector<Square>& position);
};

