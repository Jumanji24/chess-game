#pragma once
#include <QtWidgets>
#include "Service.h"
#include "SquareGUI.h"
#include "TableModel.h"

class Game : public QWidget{
private:
	int mouseX;
	int mouseY;
	int xSize;
	int ySize;
	Service& srv;
	vector<SquareGUI*> squares;
	QPixmap* p;
	ChessPiece* piece;
	SquareGUI* square;
	QTableView* view;
	TableModel* model;
	void createBoard();
	void linkSquares1(int start,int end);
	void linkSquares2(int start,int end);
	void mousePressEvent(QMouseEvent* ev) override;
	void mouseReleaseEvent(QMouseEvent* ev) override;
	void mouseMoveEvent(QMouseEvent* ev) override;
	void paintEvent(QPaintEvent* ev) override;
	int findSquare(const int& x, const int& y);
public:
	Game(Service& srv) : srv{ srv }, p{ nullptr }, piece{ nullptr }, square{ nullptr }, model{ nullptr } {
		this->setMouseTracking(false);
		this->createBoard();
		this->setFixedWidth(700);
		this->setFixedHeight(700);
		this->loadNotation();
	}
	void loadNotation();
	void repaintBoard();
	~Game() {
		for (const auto& el : this->squares) {
			delete el;
		}
	}
};

class BoardGUI : public QWidget {
private:
	Service& srv;
	Game* chessBoard;
	void keyPressEvent(QKeyEvent* ev) override {
		if (ev->key() == Qt::Key_Backspace) {
			this->srv.doUndo();
			this->chessBoard->repaintBoard();
			this->chessBoard->loadNotation();
		}
	}
public:
	BoardGUI(Service& srv, Game* brd) : srv{ srv }, chessBoard{ brd } {}
};

class PiecePromotionGUI : public QWidget {
private:
	Service& srv;
	ChessPiece* piece;
	int idx;
	int x;
	int y;
	bool madePromotion;
	void createLayout();
	void closeEvent(QCloseEvent* ev) override;
public:
	PiecePromotionGUI(Service& srv, ChessPiece* piece, const int& idx, const int& x, const int& y) :srv{ srv }, piece{ piece }, idx{ idx }, madePromotion{ false }, x{ x }, y{ y } {
		this->createLayout();
		this->move(this->x + 350, this->y + 100);
	}
};

