#include "Domain.h"

/***********************ChessPiece**********************/
int ChessPiece::moveUp(const int& idx) {
	return idx + 8;
}

int ChessPiece::moveDown(const int& idx) {
	return idx - 8;
}

int ChessPiece::moveLeft(const int& idx) {
	return idx - 1;
}

int ChessPiece::moveRight(const int& idx) {
	return idx + 1;
}

int ChessPiece::moveDiagLeftUp(const int& idx) {
	return this->moveUp(this->moveLeft(idx));
}

int ChessPiece::moveDiagRightUp(const int& idx) {
	return this->moveUp(this->moveRight(idx));
}

int ChessPiece::moveDiagLeftDown(const int& idx) {
	return this->moveDown(this->moveLeft(idx));
}

int ChessPiece::moveDiagRightDown(const int& idx) {
	return this->moveDown(this->moveRight(idx));
}

/***********************Pawn**********************/

vector<int> Pawn::getSquares(vector<Square>& tabla) {
	vector<int> squares;
	int newIdx;
	if (this->getColor() == "White") {
		newIdx = this->moveUp(this->getIndex());
		if (this->validateIndex(this->getIndex())) {
			if (tabla[newIdx].getPiece() == nullptr) {
				squares.push_back(newIdx);
			}
		}
		newIdx = this->moveDiagLeftUp(this->getIndex());
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 < this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() != nullptr && tabla[newIdx].getPiece()->getColor() == "Black") {
					squares.push_back(newIdx);
				}
			}
		}
		newIdx = this->moveDiagRightUp(this->getIndex());
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 > this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() != nullptr && tabla[newIdx].getPiece()->getColor() == "Black") {
					squares.push_back(newIdx);
				}
			}
		}
		auto potentialPawn = tabla[this->getIndex() - 1].getPiece();
		if (this->getIndex() / 8 == 4 && potentialPawn != nullptr && potentialPawn->getType() == "Pawn" && potentialPawn->getColor() == "Black" && potentialPawn->getNumOfMoves() == 1 && tabla[this->moveDiagLeftUp(this->getIndex())].getPiece() == nullptr) {
			squares.push_back(this->moveDiagLeftUp(this->getIndex()));
		}
		potentialPawn = tabla[this->getIndex() + 1].getPiece();
		if (this->getIndex() / 8 == 4 && potentialPawn != nullptr && potentialPawn->getType() == "Pawn" && potentialPawn->getColor() == "Black" && potentialPawn->getNumOfMoves() == 1 && tabla[this->moveDiagRightUp(this->getIndex())].getPiece() == nullptr) {
			squares.push_back(this->moveDiagRightUp(this->getIndex()));
		}
		if (!this->hasMoved()) {
			int newIdx1 = this->moveUp(this->getIndex());
			if (tabla[newIdx1].getPiece() == nullptr) {
				newIdx = this->moveUp(this->moveUp(this->getIndex()));
				if (tabla[newIdx].getPiece() == nullptr) {
					squares.push_back(newIdx);
				}
			}
		}
	}
	else {
		newIdx = this->moveDown(this->getIndex());
		if (this->validateIndex(newIdx)) {
			if (tabla[newIdx].getPiece() == nullptr) {
				squares.push_back(newIdx);
			}
		}
		newIdx = this->moveDiagLeftDown(this->getIndex());
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 < this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() != nullptr && tabla[newIdx].getPiece()->getColor() == "White") {
					squares.push_back(newIdx);
				}
			}
		}
		newIdx = this->moveDiagRightDown(this->getIndex());
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 > this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() != nullptr && tabla[newIdx].getPiece()->getColor() == "White") {
					squares.push_back(newIdx);
				}
			}
		}
		auto potentialPawn = tabla[this->getIndex() - 1].getPiece();
		if (this->getIndex() / 8 == 3 && potentialPawn != nullptr && potentialPawn->getType() == "Pawn" && potentialPawn->getColor() == "White" && potentialPawn->getNumOfMoves() == 1 && tabla[this->moveDiagLeftDown(this->getIndex())].getPiece() == nullptr) {
			squares.push_back(this->moveDiagLeftDown(this->getIndex()));
		}
		potentialPawn = tabla[this->getIndex() + 1].getPiece();
		if (this->getIndex() / 8 == 3 && potentialPawn != nullptr && potentialPawn->getType() == "Pawn" && potentialPawn->getColor() == "White" && potentialPawn->getNumOfMoves() == 1 && tabla[this->moveDiagRightDown(this->getIndex())].getPiece() == nullptr) {
			squares.push_back(this->moveDiagRightDown(this->getIndex()));
		}
		if (!this->hasMoved()) {
			int newIdx1 = this->moveDown(this->getIndex());
			if (tabla[newIdx1].getPiece() == nullptr) {
				newIdx = this->moveDown(this->moveDown(this->getIndex()));
				if (this->validateIndex(newIdx)) {
					if (tabla[newIdx].getPiece() == nullptr) {
						squares.push_back(newIdx);
					}
				}
			}
		}
	}
	return squares;
}

/***********************Knight**********************/
vector<int> Knight::getSquares(vector<Square>& tabla) {
	vector<int> squares;
	int newIdx = this->moveRight(this->moveRight(this->moveUp(this->getIndex())));
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 > this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveLeft(this->moveLeft(this->moveUp(this->getIndex())));
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 < this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveRight(this->moveRight(this->moveDown(this->getIndex())));
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 > this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveLeft(this->moveLeft(this->moveDown(this->getIndex())));
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 < this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveUp(this->moveUp(this->moveRight(this->getIndex())));
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 > this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveUp(this->moveUp(this->moveLeft(this->getIndex())));
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 < this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveDown(this->moveDown(this->moveRight(this->getIndex())));
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 > this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveDown(this->moveDown(this->moveLeft(this->getIndex())));
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 < this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	return squares;
}


/***********************Bishop**********************/
vector<int> Bishop::getSquares(vector<Square>& tabla) {
	vector<int> squares;
	//move diag right up
	auto newIdx = this->getIndex();
	for (int i = 0; i < 7; i++) {
		newIdx = this->moveDiagRightUp(newIdx);
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 > this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() == nullptr) {

					squares.push_back(newIdx);
				}
				else {
					if (tabla[newIdx].getPiece()->getColor() != this->getColor()) {
						squares.push_back(newIdx);

					}
					break;
				}

			}
		}
	}
	//move diag right down
	newIdx = this->getIndex();
	for (int i = 0; i < 7; i++) {
		newIdx = this->moveDiagRightDown(newIdx);
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 > this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() == nullptr) {

					squares.push_back(newIdx);
				}
				else {
					if (tabla[newIdx].getPiece()->getColor() != this->getColor()) {
						squares.push_back(newIdx);

					}
					break;
				}

			}
		}
	}
	//move diag left up
	newIdx = this->getIndex();
	for (int i = 0; i < 7; i++) {
		newIdx = this->moveDiagLeftUp(newIdx);
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 < this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() == nullptr) {

					squares.push_back(newIdx);
				}
				else {
					if (tabla[newIdx].getPiece()->getColor() != this->getColor()) {
						squares.push_back(newIdx);

					}
					break;
				}

			}
		}
	}
	//move diag left down
	newIdx = this->getIndex();
	for (int i = 0; i < 7; i++) {
		newIdx = this->moveDiagLeftDown(newIdx);
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 < this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() == nullptr) {

					squares.push_back(newIdx);
				}
				else {
					if (tabla[newIdx].getPiece()->getColor() != this->getColor()) {
						squares.push_back(newIdx);

					}
					break;
				}

			}
		}
	}
	return squares;
}

/***********************Rook**********************/
vector<int> Rook::getSquares(vector<Square>& tabla) {
	vector<int> squares;
	//move right
	auto newIdx = this->getIndex();
	for (int i = 0; i < 7; i++) {
		newIdx = this->moveRight(newIdx);
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 > this->getIndex() % 8) {

				if (tabla[newIdx].getPiece() == nullptr) {

					squares.push_back(newIdx);
				}
				else {
					if (tabla[newIdx].getPiece()->getColor() != this->getColor()) {
						squares.push_back(newIdx);

					}
					break;
				}
			}
		}
	}
	//move left
	newIdx = this->getIndex();
	for (int i = 0; i < 7; i++) {
		newIdx = this->moveLeft(newIdx);
		if (this->validateIndex(newIdx)) {
			if (newIdx % 8 < this->getIndex() % 8) {
				if (tabla[newIdx].getPiece() == nullptr) {

					squares.push_back(newIdx);
				}
				else {
					if (tabla[newIdx].getPiece()->getColor() != this->getColor()) {
						squares.push_back(newIdx);

					}
					break;
				}
			}
		}
	}
	//move down
	newIdx = this->getIndex();
	for (int i = 0; i < 7; i++) {
		newIdx = this->moveDown(newIdx);
		if (this->validateIndex(newIdx)) {
			if (tabla[newIdx].getPiece() == nullptr) {

				squares.push_back(newIdx);
			}
			else {
				if (tabla[newIdx].getPiece()->getColor() != this->getColor()) {
					squares.push_back(newIdx);

				}
				break;
			}
		}
	}
	//move up
	newIdx = this->getIndex();
	for (int i = 0; i < 7; i++) {
		newIdx = this->moveUp(newIdx);
		if (this->validateIndex(newIdx)) {
			if (tabla[newIdx].getPiece() == nullptr) {

				squares.push_back(newIdx);
			}
			else {
				if (tabla[newIdx].getPiece()->getColor() != this->getColor()) {
					squares.push_back(newIdx);

				}
				break;
			}
		}
	}
	return squares;
}


/***********************Queen**********************/
vector<int> Queen::getSquares(vector<Square>& tabla) {
	vector<int> squares;
	Rook r(this->getColor(), this->getIndex());
	Bishop b(this->getColor(), this->getIndex());
	squares = r.getSquares(tabla);
	for (const auto& el : b.getSquares(tabla)) {
		squares.push_back(el);
	}
	return squares;
}


/***********************King**********************/
vector<int> King::getSquares(vector<Square>& tabla) {
	vector<int> squares;
	int newIdx = this->moveUp(this->getIndex());
	if (this->validateIndex(newIdx)) {
		if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
			squares.push_back(newIdx);
		}
	}
	newIdx = this->moveDown(this->getIndex());
	if (this->validateIndex(newIdx)) {
		if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
			squares.push_back(newIdx);
		}
	}
	newIdx = this->moveLeft(this->getIndex());
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 < this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveRight(this->getIndex());
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 > this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveDiagLeftDown(this->getIndex());
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 < this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveDiagRightDown(this->getIndex());
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 > this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveDiagLeftUp(this->getIndex());
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 < this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	newIdx = this->moveDiagRightUp(this->getIndex());
	if (this->validateIndex(newIdx)) {
		if (newIdx % 8 > this->getIndex() % 8) {
			if (tabla[newIdx].getPiece() == nullptr || tabla[newIdx].getPiece()->getColor() != this->getColor()) {
				squares.push_back(newIdx);
			}
		}
	}
	if (!this->hasMoved()) {
		newIdx = this->moveRight(this->getIndex());
		if (this->validateIndex(newIdx)) {
			if (tabla[newIdx].getPiece() == nullptr) {
				int newIdx2 = this->moveRight(newIdx);
				if (this->validateIndex(newIdx2)) {
					if (tabla[newIdx2].getPiece() == nullptr) {
						int newIdx3 = this->moveRight(newIdx2);
						if (this->validateIndex(newIdx3)) {
							if (tabla[newIdx3].getPiece() != nullptr && tabla[newIdx3].getPiece()->getType() == "Rook" && tabla[newIdx3].getPiece()->getColor() == this->getColor() && !tabla[newIdx3].getPiece()->hasMoved()) {
								squares.push_back(newIdx2);
							}
						}
					}
				}
			}
		}
		newIdx = this->moveLeft(this->getIndex());
		if (this->validateIndex(newIdx)) {
			if (tabla[newIdx].getPiece() == nullptr) {
				int newIdx2 = this->moveLeft(newIdx);
				if(this->validateIndex(newIdx2)){
					if (tabla[newIdx2].getPiece() == nullptr) {
						int newIdx3 = this->moveLeft(newIdx2);
						if (this->validateIndex(newIdx3)) {
							if (tabla[newIdx3].getPiece() == nullptr) {
								int newIdx4 = this->moveLeft(newIdx3);
								if (this->validateIndex(newIdx4)) {
									if (tabla[newIdx4].getPiece() != nullptr && tabla[newIdx4].getPiece()->getType() == "Rook" && tabla[newIdx4].getPiece()->getColor() == this->getColor() && !tabla[newIdx4].getPiece()->hasMoved()) {
										squares.push_back(newIdx2);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return squares;
}

//util function to make a copy piece
ChessPiece* createPieceCopy(ChessPiece* piece) {
	ChessPiece* pp = nullptr;
	if (piece != nullptr) {
		if (piece->getType() == "Pawn") {
			pp = new Pawn(piece->getColor(), piece->getIndex());
			pp->setOldIdx(piece->getOldIdx());
			/*if (pp->getIndex() / 8 != 1 && pp->getColor() == "White" || pp->getIndex() / 8 != 6 && pp->getColor() == "Black") {
				pp->setMoved();
			}*/
			if (piece->hasMoved()) {
				pp->setMoved();
			}
			//pp->setNumOfMoves(piece->getNumOfMoves());
		}
		if (piece->getType() == "Knight") {
			pp = new Knight(piece->getColor(), piece->getIndex());
			pp->setOldIdx(piece->getOldIdx());
			if (piece->hasMoved()) {
				pp->setMoved();
			}
		}
		if (piece->getType() == "Bishop") {
			pp = new Bishop(piece->getColor(), piece->getIndex());
			pp->setOldIdx(piece->getOldIdx());
			if (piece->hasMoved()) {
				pp->setMoved();
			}
		}
		if (piece->getType() == "Queen") {
			pp = new Queen(piece->getColor(), piece->getIndex());
			pp->setOldIdx(piece->getOldIdx());
			if (piece->hasMoved()) {
				pp->setMoved();
			}
		}
		if (piece->getType() == "King") {
			pp = new King(piece->getColor(), piece->getIndex());
			pp->setOldIdx(piece->getOldIdx());
			if (piece->hasMoved()) {
				pp->setMoved();
			}
		}
		if (piece->getType() == "Rook") {
			pp = new Rook(piece->getColor(), piece->getIndex());
			pp->setOldIdx(piece->getOldIdx());
			if (piece->hasMoved()) {
				pp->setMoved();
			}
		}
	}
	return pp;
}