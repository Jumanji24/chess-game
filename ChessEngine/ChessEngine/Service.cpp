#include "Service.h"
#include <algorithm>
#include <QtWidgets>

vector<Square>& Service::getBoard() {
	return this->repo.getBoard();
}

bool Service::updateBoard(ChessPiece*& piece, const int& idx) {
	if (piece != nullptr) {

		bool wasInCheck = false;
		if (piece->getType() == "King") {
			wasInCheck = this->isInCheck(piece);
		}
		if ((this->whosMovin == 0 && piece->getColor() == "White") || this->whosMovin == 1 && piece->getColor() == "Black") {
			UndoMove u{ piece,this->repo.getBoard()[idx].getPiece(),idx };
			int idx2 = piece->getIndex();
			bool correctMove = this->repo.updateBoard(piece, idx);
			if (correctMove) {
				piece->incNumOfMoves();
				this->changePlayer();
				this->undoList.push_back(u);
				/*if ((piece->getColor() == "White" && this->isInCheck(this->repo.getWhiteKing()) == true) || (piece->getColor() == "Black" && this->isInCheck(this->repo.getBlackKing()) == true)) {
					this->doUndo();
					return false;
				}*/
				piece->setMoved();
				bool madeCastle = false;
				//short castle
				if (piece->getType() == "King" && piece->getIndex() - idx2 == 2) {
					this->repo.setOnBoard(this->repo.getBoard()[piece->getIndex() + 1].getPiece(), piece->getIndex() - 1);
					ChessPiece* kng = new King{ piece->getColor(),idx2 + 1 };
					if (this->isInCheck(kng)) {
						auto pp = createPieceCopy(piece);
						this->doUndo();
						if (pp->getColor() == "Black") {
							piece = this->repo.getBlackKing();
						}
						else {
							piece = this->repo.getWhiteKing();
						}
						delete pp;
						delete kng;
						madeCastle = true;
						return false;
					}
				}
				//long castle
				if (piece->getType() == "King" && piece->getIndex() - idx2 == -2) {
					this->repo.setOnBoard(this->repo.getBoard()[piece->getIndex() - 2].getPiece(), piece->getIndex() + 1);
					ChessPiece* kng = new King{ piece->getColor(),idx2 - 1 };
					if (this->isInCheck(kng)) {
						auto pp = createPieceCopy(piece);
						this->doUndo();
						if (pp->getColor() == "Black") {
							piece = this->repo.getBlackKing();
						}
						else {
							piece = this->repo.getWhiteKing();
						}
						delete pp;
						delete kng;
						madeCastle = true;
						return false;
					}
				}
				if (wasInCheck && madeCastle) {
					this->doUndo();
					if (piece->getColor() == "Black") {
						piece = this->repo.getBlackKing();
					}
					else {
						piece = this->repo.getWhiteKing();
					}
					return false;
				}


			}
			return correctMove;
		}
		return false;
	}

	return false;
}

bool Service::isInCheck(ChessPiece* kng) {
	//************************CAZ SAH-DAMA-TURN************************************
	int idx = kng->getIndex();
	for (int i = 0; i < 7; i++) {
		idx = kng->moveUp(idx);
		if (kng->validateIndex(idx)) {
			auto piesa = this->repo.getBoard()[idx].getPiece();
			if (piesa != nullptr) {
				if (piesa->getColor() != kng->getColor()) {
					if (piesa->getType() == "Queen" || piesa->getType() == "Rook")
						return true;

				}
				break;
			}
		}
	}
	idx = kng->getIndex();
	for (int i = 0; i < 7; i++) {
		idx = kng->moveDown(idx);
		if (kng->validateIndex(idx)) {
			auto piesa = this->repo.getBoard()[idx].getPiece();
			if (piesa != nullptr) {
				if (piesa->getColor() != kng->getColor()) {
					if (piesa->getType() == "Queen" || piesa->getType() == "Rook")
						return true;
				}
				break;

			}
		}
	}
	idx = kng->getIndex();
	for (int i = 0; i < 7; i++) {
		idx = kng->moveLeft(idx);
		if (kng->validateIndex(idx)) {
			if (idx % 8 < kng->getIndex() % 8) {
				auto piesa = this->repo.getBoard()[idx].getPiece();
				if (piesa != nullptr) {
					if (piesa->getColor() != kng->getColor()) {
						if (piesa->getType() == "Queen" || piesa->getType() == "Rook")
							return true;
					}
					break;
				}
			}
		}
	}
	idx = kng->getIndex();
	for (int i = 0; i < 7; i++) {
		idx = kng->moveRight(idx);
		if (kng->validateIndex(idx)) {
			if (idx % 8 > kng->getIndex() % 8) {
				auto piesa = this->repo.getBoard()[idx].getPiece();
				if (piesa != nullptr) {
					if (piesa->getColor() != kng->getColor()) {
						if (piesa->getType() == "Queen" || piesa->getType() == "Rook")
							return true;
					}
					break;
				}
			}
		}
	}
	//******************CAZ SAH-DAMA-NEBUN******************
	idx = kng->getIndex();
	for (int i = 0; i < 7; i++) {
		idx = kng->moveDiagLeftUp(idx);
		if (kng->validateIndex(idx)) {
			if (idx % 8 < kng->getIndex() % 8) {
				auto piesa = this->repo.getBoard()[idx].getPiece();
				if (piesa != nullptr) {
					if (piesa->getColor() != kng->getColor()) {
						if (piesa->getType() == "Queen" || piesa->getType() == "Bishop")
							return true;
					}
					break;
				}
			}
		}
	}
	idx = kng->getIndex();
	for (int i = 0; i < 7; i++) {
		idx = kng->moveDiagLeftDown(idx);
		if (kng->validateIndex(idx)) {
			if (idx % 8 < kng->getIndex() % 8) {
				auto piesa = this->repo.getBoard()[idx].getPiece();
				if (piesa != nullptr) {
					if (piesa->getColor() != kng->getColor()) {
						if (piesa->getType() == "Queen" || piesa->getType() == "Bishop")
							return true;
					}
					break;
				}
			}
		}
	}
	idx = kng->getIndex();
	for (int i = 0; i < 7 - (kng->getIndex() % 8); i++) {
		idx = kng->moveDiagRightUp(idx);
		if (kng->validateIndex(idx)) {
			if (idx % 8 > kng->getIndex() % 8) {
				auto piesa = this->repo.getBoard()[idx].getPiece();
				if (piesa != nullptr) {
					if (piesa->getColor() != kng->getColor()) {
						if (piesa->getType() == "Queen" || piesa->getType() == "Bishop")
							return true;
					}
					break;
				}
			}
		}
	}
	idx = kng->getIndex();
	for (int i = 0; i < 7; i++) {
		idx = kng->moveDiagRightDown(idx);
		if (kng->validateIndex(idx)) {
			if (idx % 8 > kng->getIndex() % 8) {
				auto piesa = this->repo.getBoard()[idx].getPiece();
				if (piesa != nullptr) {
					if (piesa->getColor() != kng->getColor()) {
						if (piesa->getType() == "Queen" || piesa->getType() == "Bishop")
							return true;
					}
					break;
				}
			}
		}
	}
	//*********************************CAZ SAH-PION***************************
	if (kng->getColor() == "White") {
		idx = kng->getIndex();
		if (kng->validateIndex(kng->moveDiagLeftUp(idx))) {
			auto piesa = repo.getBoard()[kng->moveDiagLeftUp(idx)].getPiece();
			if (piesa != nullptr) {
				if (piesa->getType() == "Pawn" && piesa->getColor() == "Black") {
					return true;
				}

			}
		}
		if (kng->validateIndex(kng->moveDiagRightUp(idx))) {
			auto piesa = this->repo.getBoard()[kng->moveDiagRightUp(idx)].getPiece();
			if (piesa != nullptr) {
				if (piesa->getType() == "Pawn" && piesa->getColor() == "Black") {
					return true;
				}
			}
		}
	}
	else {
		idx = kng->getIndex();
		if (kng->validateIndex(kng->moveDiagLeftDown(idx))) {
			auto piesa = this->repo.getBoard()[kng->moveDiagLeftDown(idx)].getPiece();
			if (piesa != nullptr) {
				if (piesa->getType() == "Pawn" && piesa->getColor() == "White") {
					return true;
				}
			}
		}
		if (kng->validateIndex(kng->moveDiagRightDown(idx))) {
			auto piesa = this->repo.getBoard()[kng->moveDiagRightDown(idx)].getPiece();
			if (piesa != nullptr) {
				if (piesa->getType() == "Pawn" && piesa->getColor() == "White") {
					return true;
				}
			}
		}
	}


	//*******************CAZ SAH-CAL*******************
	idx = kng->getIndex();
	Knight k;
	k.setIndex(idx);
	auto v = k.getSquares(this->repo.getBoard());
	for (const auto& el : v) {
		auto piesa = this->repo.getBoard()[el].getPiece();
		if (piesa != nullptr) {
			if (piesa->getType() == "Knight" && piesa->getColor() != kng->getColor())
				return true;
		}
	}
	//*******************CAZ SAH-Rege*******************
	idx = kng->getIndex();
	v = kng->getSquares(this->repo.getBoard());
	for (const auto& el : v) {
		if (this->repo.getBoard()[el].getPiece() != nullptr && this->repo.getBoard()[el].getPiece()->getType() == "King") {
			return true;
		}
	}
	return false;

}

void Service::changePlayer() {
	if (this->whosMovin == 0) {
		this->whosMovin++;
	}
	else {
		this->whosMovin = 0;
	}
}

void Service::doUndo() {
	if (this->undoList.size() > 0) {
		auto undos = this->undoList.back();
		this->undoList.pop_back();
		delete this->repo.getBoard()[undos.idx].getPiece();

		this->repo.getBoard()[undos.idx].setPiece(nullptr);
		this->repo.setOnBoard(undos.p1, undos.p1->getIndex());

		if (undos.p1->getType() == "King") {
			//undo for rook after short castle
			if (undos.p1->getIndex() - undos.idx == -2) {
				this->repo.setOnBoard(this->repo.getBoard()[undos.idx - 1].getPiece(), undos.idx + 1);
			}
			//undo for rook after long castle
			else if (undos.p1->getIndex() - undos.idx == 2) {
				this->repo.setOnBoard(this->repo.getBoard()[undos.idx + 1].getPiece(), undos.idx - 2);
			}
			if (undos.p1->getColor() == "White") {
				this->repo.setWhiteKing(undos.p1);
			}
			else {
				this->repo.setBlackKing(undos.p1);
			}
		}
		if (undos.p2 != nullptr) {
			this->repo.setOnBoard(undos.p2, undos.p2->getIndex());
		}
		this->changePlayer();

		//undo for notation
		if (undos.p1->getColor() == "White") {
			this->repo.getMoves().pop_back();
		}
		else {
			this->repo.getMoves().back().second = "";
		}
	}
}

void Service::clearBoard() {
	this->repo.clearBoard();
}

MoveDTO Service::recMiniMax(Service& srv, ChessPiece* p, int depth, int alfa, int beta, int whosMovin) {
	if (depth == 0) {
		return MoveDTO{ nullptr,this->evalPos(srv.getBoard()) };
	}
	if (whosMovin == 0) {
		double maxEval = -32000;
		ChessPiece* pp = nullptr;
		int ok = 0;
		for (const auto& sq : srv.getBoard()) {
			if (ok == 1) {
				break;
			}
			if (sq.getPiece() != nullptr) {
				if (sq.getPiece()->getColor() == "White") {
					for (const auto& idx : sq.getPiece()->getSquares(srv.getBoard())) {
						auto piece = sq.getPiece();
						int ok2 = 0;
						if (srv.updateBoard(piece, idx)) {
							auto pi = createPieceCopy(piece);
							auto move = recMiniMax(srv, pi, depth - 1, alfa, beta, 1);
							if (move.getEval() > maxEval) {
								maxEval = move.getEval();
								ok2 = 1;
							}
							if (alfa > move.getEval()) {
								alfa = move.getEval();
							}
							if (ok2 == 1) {
								pp = createPieceCopy(pi);
							}
							srv.doUndo();
							delete pi;
							if (beta < alfa) {
								ok = 1;
								break;
							}
						}
					}
				}
			}
		}
		return MoveDTO{ pp,maxEval };
	}
	else {
		double minEval = 32000;
		ChessPiece* pp = nullptr;
		int ok = 0;
		for (const auto& sq : srv.getBoard()) {
			if (ok == 1) {
				break;
			}
			if (sq.getPiece() != nullptr) {
				if (sq.getPiece()->getColor() == "Black") {
					for (const auto& idx : sq.getPiece()->getSquares(srv.getBoard())) {
						auto piece = sq.getPiece();
						int ok2 = 0;
						if (srv.updateBoard(piece, idx)) {
							auto pi = createPieceCopy(piece);
							auto move = recMiniMax(srv, piece, depth - 1, alfa, beta, 0);
							if (move.getEval() < minEval) {
								minEval = move.getEval();
								ok2 = 1;
							}
							if (beta < move.getEval()) {
								beta = move.getEval();
							}
							if (ok2 == 1) {
								pp = createPieceCopy(pi);
							}
							srv.doUndo();
							delete pi;
							if (beta < alfa) {
								ok = 1;
								break;
							}
						}
					}
				}
			}
		}
		return MoveDTO{ pp,minEval };
	}
}

void Service::miniMax(int depth, int alfa, int beta, int whosMovin) {
	Repository repo;
	Service srv2{ repo };
	if (srv2.getWhosMovin() != this->whosMovin) {
		srv2.changePlayer();
	}
	srv2.repo.setBoard(this->repo.getBoard());
	srv2.repo.setNotation(this->repo.getMoves());
	srv2.repo.setBlackKing(createPieceCopy(this->repo.getBlackKing()));
	srv2.repo.setWhiteKing(createPieceCopy(this->repo.getWhiteKing()));
	auto move = this->recMiniMax(srv2, nullptr, depth, alfa, beta, whosMovin);
	for (const auto& sq : this->repo.getBoard()) {
		if (sq.getPiece() != nullptr) {
			auto piece = sq.getPiece();
			if (piece->getColor() == move.getPiece()->getColor() && piece->getType() == move.getPiece()->getType() && piece->getIndex() == move.getPiece()->getOldIdx()) {
				this->updateBoard(piece, move.getPiece()->getIndex());
				delete move.getPiece();
				break;
			}
		}
	}
}

double Service::evalPos(vector<Square>& position) {
	double val = 0;
	for (const auto& el : position) {
		auto piece = el.getPiece();
		if (piece != nullptr && piece->getColor() == "White") {
			val += piece->getValue() + piece->getSqareValues().at(piece->getIndex());;
		}
		else if (piece != nullptr && piece->getColor() == "Black") {
			val -= piece->getValue() + piece->getSqareValues().at(piece->getIndex());
		}
	}
	return val;
}
