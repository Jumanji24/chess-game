#include "Game.h"

void Game::linkSquares1(int start, int end) {
	for (int i = start; i < end; i++) {
		if (i % 2 == 0) {
			this->squares.push_back(new SquareGUI(this->srv.getBoard()[i], "D:/Projects/ChessEngine/ChessEngine/PNG/BlackSquare.png"));
		}
		else {
			this->squares.push_back(new SquareGUI(this->srv.getBoard()[i], "D:/Projects/ChessEngine/ChessEngine/PNG/WhiteSquare.png"));
		}
	}
}

void Game::linkSquares2(int start, int end) {
	for (int i = start; i < end; i++) {
		if (i % 2 != 0) {
			this->squares.push_back(new SquareGUI(this->srv.getBoard()[i], "D:/Projects/ChessEngine/ChessEngine/PNG/BlackSquare.png"));
		}
		else {
			this->squares.push_back(new SquareGUI(this->srv.getBoard()[i], "D:/Projects/ChessEngine/ChessEngine/PNG/WhiteSquare.png"));
		}
	}
}

void Game::createBoard() {
	auto board = new BoardGUI(this->srv, this);
	auto mainLay = new QHBoxLayout();
	auto boardLay = new QVBoxLayout();
	boardLay->setSpacing(0);

	this->linkSquares1(0, 8);
	this->linkSquares2(8, 16);
	this->linkSquares1(16, 24);
	this->linkSquares2(24, 32);
	this->linkSquares1(32, 40);
	this->linkSquares2(40, 48);
	this->linkSquares1(48, 56);
	this->linkSquares2(56, 64);

	auto rowLay1 = new QHBoxLayout();
	rowLay1->setSpacing(0);
	for (int i = 0; i < 8; i++) {
		rowLay1->addWidget(this->squares[i]);
	}
	auto rowLay2 = new QHBoxLayout();
	rowLay2->setSpacing(0);
	for (int i = 8; i < 16; i++) {
		rowLay2->addWidget(this->squares[i]);
	}
	auto rowLay3 = new QHBoxLayout();
	rowLay3->setSpacing(0);
	for (int i = 16; i < 24; i++) {
		rowLay3->addWidget(this->squares[i]);
	}
	auto rowLay4 = new QHBoxLayout();
	rowLay4->setSpacing(0);
	for (int i = 24; i < 32; i++) {
		rowLay4->addWidget(this->squares[i]);
	}
	auto rowLay5 = new QHBoxLayout();
	rowLay5->setSpacing(0);
	for (int i = 32; i < 40; i++) {
		rowLay5->addWidget(this->squares[i]);
	}
	auto rowLay6 = new QHBoxLayout();
	rowLay6->setSpacing(0);
	for (int i = 40; i < 48; i++) {
		rowLay6->addWidget(this->squares[i]);
	}
	auto rowLay7 = new QHBoxLayout();
	rowLay7->setSpacing(0);
	for (int i = 48; i < 56; i++) {
		rowLay7->addWidget(this->squares[i]);
	}
	auto rowLay8 = new QHBoxLayout();
	rowLay8->setSpacing(0);
	for (int i = 56; i < 64; i++) {
		rowLay8->addWidget(this->squares[i]);
	}

	boardLay->addLayout(rowLay8);
	boardLay->addLayout(rowLay7);
	boardLay->addLayout(rowLay6);
	boardLay->addLayout(rowLay5);
	boardLay->addLayout(rowLay4);
	boardLay->addLayout(rowLay3);
	boardLay->addLayout(rowLay2);
	boardLay->addLayout(rowLay1);
	mainLay->addLayout(boardLay);

	auto notationLayout = new QVBoxLayout();
	this->view = new QTableView;
	notationLayout->addWidget(view);
	mainLay->addLayout(notationLayout);

	board->setFixedWidth(1250);
	board->setFixedHeight(700);
	board->setLayout(mainLay);
	this->setParent(board);
	board->show();
}

int Game::findSquare(const int& x, const int& y) {
	double dist = 10000;
	int j = -1;
	for (int i = 0; i < this->squares.size(); i++) {
		if (x > this->squares[i]->x() && y > this->squares[i]->y()) {
			double newDist = sqrt(pow(x - this->squares[i]->x(), 2) + pow(y - this->squares[i]->y(), 2));
			if (newDist < dist) {
				dist = newDist;
				j = i;
			}
		}
	}
	return j;
}


void Game::mousePressEvent(QMouseEvent* ev) {
	if (ev->button() == Qt::LeftButton) {
		this->setMouseTracking(true);
	}
	else if (ev->button() == Qt::RightButton) {
		this->srv.miniMax(3, -32000, 32000, this->srv.getWhosMovin());
		this->repaintBoard();
		this->loadNotation();
	}
}

void Game::mouseReleaseEvent(QMouseEvent* ev) {
	if (ev->button() == Qt::LeftButton) {
		this->setMouseTracking(false);
		this->p = nullptr;
		auto a = ev->x();
		auto b = ev->y();
		int i = this->findSquare(ev->x(), ev->y());
		if (i == -1) {
			if (this->square != nullptr) {
				this->square->getSquare().setPiece(this->piece);
			}
		}
		else {
			if (this->piece != nullptr) {
				auto idx = this->piece->getIndex();
				auto deepPiece = createPieceCopy(this->piece);
				if (this->srv.updateBoard(deepPiece, i)) {
					this->piece = squares[i]->getSquare().getPiece();
					//promotion
					if ((i >= 56 && i <= 63 && piece->getType() == "Pawn") || (i >= 0 && i <= 7 && piece->getType() == "Pawn")) {
						auto ppg = new PiecePromotionGUI(this->srv, this->piece, i, ev->x(), ev->y());
						ppg->show();
					}
				}
				else {
					this->square->getSquare().setPiece(this->piece);
				}
			}
		}
		this->loadNotation();
		this->repaintBoard();
		this->square = nullptr;
		this->piece = nullptr;
		//ai moves for black
		/*if (this->srv.getWhosMovin() == 1) {
			this->srv.miniMax(3, -32000, 32000, this->srv.getWhosMovin());
			this->repaintBoard();
			this->loadNotation();
		}*/
	}
}

void Game::mouseMoveEvent(QMouseEvent* ev) {
	int i = this->findSquare(ev->x(), ev->y());
	if (i != -1) {
		if (this->squares[i]->getSquare().getPiece() != nullptr) {
			if (this->p == nullptr && this->piece == nullptr) {
				this->p = this->squares[i]->getPixPiece();
				this->square = this->squares[i];
				this->piece = this->squares[i]->getSquare().getPiece();
				this->xSize = this->squares[i]->getXSize();
				this->ySize = this->squares[i]->getYSize();
				//qDebug() << QString::fromStdString(this->squares[i]->getSquare().getPiece()->getColor()) + " " + QString::fromStdString(this->squares[i]->getSquare().getPiece()->getType());
				this->squares[i]->getSquare().setPiece(nullptr);
			}
		}
		this->mouseX = ev->x();
		this->mouseY = ev->y();
		this->repaint();
	}
}

void Game::paintEvent(QPaintEvent* ev) {
	if (this->p != nullptr) {
		QPainter painter(this);
		painter.drawPixmap(this->mouseX - this->xSize / 2, this->mouseY - this->ySize / 2, this->xSize, ySize, *this->p);
	}
}

void Game::repaintBoard() {
	for (const auto& el : this->squares) {
		el->repaint();
	}
}

void Game::loadNotation() {
	delete this->model;
	this->model = new TableModel(this->srv.getMoves());
	this->view->setModel(this->model);
}

void PiecePromotionGUI::createLayout() {
	auto mainLay = new QVBoxLayout();
	mainLay->setSpacing(0);
	auto upLay = new QHBoxLayout();
	upLay->setSpacing(0);
	auto downLay = new QHBoxLayout();
	downLay->setSpacing(0);
	if (this->piece->getColor() == "White") {
		auto knightBtn = new QToolButton();
		knightBtn->setIcon(QIcon("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteKnight.png"));
		knightBtn->setIconSize(QSize(50, 50));
		QObject::connect(knightBtn, &QToolButton::clicked, [&]() {
			this->srv.getBoard()[this->piece->getIndex()].setPiece(nullptr);
			delete this->piece;
			auto newPiece = new Knight("White", idx);
			this->srv.setOnBoard(newPiece, idx);
			this->madePromotion = true;
			this->close();
			});
		auto bishopBtn = new QToolButton();
		bishopBtn->setIcon(QIcon("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteBishop.png"));
		bishopBtn->setIconSize(QSize(50, 50));
		QObject::connect(bishopBtn, &QToolButton::clicked, [&]() {
			this->srv.getBoard()[this->piece->getIndex()].setPiece(nullptr);
			delete this->piece;
			auto newPiece = new Bishop("White", idx);
			this->srv.setOnBoard(newPiece, idx);
			this->madePromotion = true;
			this->close();
			});
		auto rookBtn = new QToolButton();
		rookBtn->setIcon(QIcon("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteRook.png"));
		rookBtn->setIconSize(QSize(50, 50));
		QObject::connect(rookBtn, &QToolButton::clicked, [&]() {
			this->srv.getBoard()[this->piece->getIndex()].setPiece(nullptr);
			delete this->piece;
			auto newPiece = new Rook("White", idx);
			this->srv.setOnBoard(newPiece, idx);
			this->madePromotion = true;
			this->close();
			});
		auto queenBtn = new QToolButton();
		queenBtn->setIcon(QIcon("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteQueen.png"));
		queenBtn->setIconSize(QSize(50, 50));
		QObject::connect(queenBtn, &QToolButton::clicked, [&]() {
			this->srv.getBoard()[this->piece->getIndex()].setPiece(nullptr);
			delete this->piece;
			auto newPiece = new Queen("White", idx);
			this->srv.setOnBoard(newPiece, idx);
			this->madePromotion = true;
			this->close();
			});
		upLay->addWidget(knightBtn);
		upLay->addWidget(bishopBtn);
		downLay->addWidget(rookBtn);
		downLay->addWidget(queenBtn);
	}
	else {
		auto knightBtn = new QToolButton();
		knightBtn->setIcon(QIcon("D:/Projects/ChessEngine/ChessEngine/PNG/BlackKnight.png"));
		knightBtn->setIconSize(QSize(50, 50));
		QObject::connect(knightBtn, &QToolButton::clicked, [&]() {
			this->srv.getBoard()[this->piece->getIndex()].setPiece(nullptr);
			delete this->piece;
			auto newPiece = new Knight("Black", idx);
			this->srv.setOnBoard(newPiece, idx);
			this->madePromotion = true;
			this->close();
			});
		auto bishopBtn = new QToolButton();
		bishopBtn->setIcon(QIcon("D:/Projects/ChessEngine/ChessEngine/PNG/BlackBishop.png"));
		bishopBtn->setIconSize(QSize(50, 50));
		QObject::connect(bishopBtn, &QToolButton::clicked, [&]() {
			this->srv.getBoard()[this->piece->getIndex()].setPiece(nullptr);
			delete this->piece;
			auto newPiece = new Bishop("Black", idx);
			this->srv.setOnBoard(newPiece, idx);
			this->madePromotion = true;
			this->close();
			});
		auto rookBtn = new QToolButton();
		rookBtn->setIcon(QIcon("D:/Projects/ChessEngine/ChessEngine/PNG/BlackRook.png"));
		rookBtn->setIconSize(QSize(50, 50));
		QObject::connect(rookBtn, &QToolButton::clicked, [&]() {
			this->srv.getBoard()[this->piece->getIndex()].setPiece(nullptr);
			delete this->piece;
			auto newPiece = new Rook("Black", idx);
			this->srv.setOnBoard(newPiece, idx);
			this->madePromotion = true;
			this->close();
			});
		auto queenBtn = new QToolButton();
		queenBtn->setIcon(QIcon("D:/Projects/ChessEngine/ChessEngine/PNG/BlackQueen.png"));
		queenBtn->setIconSize(QSize(50, 50));
		QObject::connect(queenBtn, &QToolButton::clicked, [&]() {
			this->srv.getBoard()[this->piece->getIndex()].setPiece(nullptr);
			delete this->piece;
			auto newPiece = new Queen("Black", idx);
			this->srv.setOnBoard(newPiece, idx);
			this->madePromotion = true;
			this->close();
			});
		upLay->addWidget(knightBtn);
		upLay->addWidget(bishopBtn);
		downLay->addWidget(rookBtn);
		downLay->addWidget(queenBtn);
	}
	mainLay->addLayout(upLay);
	mainLay->addLayout(downLay);
	this->setLayout(mainLay);
}

void PiecePromotionGUI::closeEvent(QCloseEvent* ev) {
	if (!this->madePromotion) {
		this->srv.doUndo();
	}
	ev->accept();
}