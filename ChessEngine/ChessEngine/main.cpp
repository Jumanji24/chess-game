#include <QtWidgets>
#include "Game.h"
#include "Test.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Test test;
    test.testAll();
    qDebug() << "Tests passed!";
    Repository repo;
    Service srv(repo);
    Game game(srv);
    game.show();
    return a.exec();
}
