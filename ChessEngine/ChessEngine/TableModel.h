#pragma once
#include <string>
#include <vector>
#include <QtWidgets>
using namespace std;

class TableModel : public QAbstractTableModel {
private:
	vector<pair<string, string>> moves;
public:
	TableModel(vector<pair<string, string>>& moves) :moves{ moves } {}

	virtual int	rowCount(const QModelIndex& parent = QModelIndex()) const override {
		return this->moves.size();
	}

	virtual int	columnCount(const QModelIndex& parent = QModelIndex()) const override {
		return 2;
	}

	virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override {
		if (role == Qt::DisplayRole) {
			if (index.column() == 0) {
				return QString::fromStdString(this->moves[index.row()].first);
			}
			else if (index.column() == 1) {
				return QString::fromStdString(this->moves[index.row()].second);
			}
		}
		return QVariant();
	}

};

