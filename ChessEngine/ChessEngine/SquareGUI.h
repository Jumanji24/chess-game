#pragma once
#include <QtWidgets>
#include "Domain.h"

class SquareGUI : public QMainWindow{
private:
	Square& square;
	string path;
	QPixmap* pixPiece;
	int xSize;
	int ySize;

public:
	SquareGUI(Square& sq, const string path) : square{ sq }, path{ path }, pixPiece{ nullptr } {}
	Square& getSquare() { return this->square; }
	void paintEvent(QPaintEvent* ev) override;
	int getXSize() const { return this->xSize; }
	int getYSize() const { return this->ySize; }
	QPixmap* getPixPiece() const { return this->pixPiece; }
	~SquareGUI() {
		delete this->pixPiece;
	}
};

