#pragma once
#include "Domain.h"
#include "map"
using namespace std;
class Repository
{
private:
	vector<Square> tabla;
	vector<pair<string, string>> moves;
	map <int, string> mapCoord;
	void initBoard();
	void initMapCoord();
	ChessPiece* whiteKing;
	ChessPiece* blackKing;
public:
	Repository() {
		this->initBoard();
		this->initMapCoord();
	}
	vector<Square>& getBoard();
	bool updateBoard(ChessPiece* piece, const int& idx);
	ChessPiece* getWhiteKing()const {
		return this->whiteKing;
	}
	ChessPiece* getBlackKing()const {
		return this->blackKing;
	}
	vector<pair<string, string>>& getMoves() {
		return this->moves;
	}
	void setBlackKing(ChessPiece* kng) {
		this->blackKing = kng;
	}
	void setWhiteKing(ChessPiece* kng) {
		this->whiteKing = kng;
	}
	void clearBoard();

	void setOnBoard(ChessPiece* piece, int idx) {
		if (tabla[idx].getPiece() != nullptr) {
			delete tabla[idx].getPiece();
		}
		tabla[piece->getIndex()].setPiece(nullptr);
		tabla[idx].setPiece(piece);
		piece->setIndex(idx);
	}
	void setBoard(vector<Square>& v);
	void setNotation(vector<pair<string, string>>& moves);
	~Repository() {
		for (const auto& el : this->tabla) {
			delete el.getPiece();
		}
	}
};

