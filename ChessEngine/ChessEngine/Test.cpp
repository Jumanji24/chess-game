#include <iostream>
#include "Test.h"
#include "Service.h"
#include "QtWidgets"
#include <assert.h>
//void tipareste(const vector<int>& v) {
//	for (const auto& el : v) {
//		qDebug() << el << " ";
//	}
//	qDebug() << "\n";
//}

void Test::testDomain() {
	ChessPiece* r = new Rook("White", 1);
	ChessPiece* n = new Knight("White", 14);
	ChessPiece* b = new Bishop("White", 36);
	ChessPiece* q = new Queen("White", 35);
	ChessPiece* k = new King("White", 2);
	ChessPiece* pw = new Pawn("White", 10);
	ChessPiece* pb = new Pawn("Black", 51);
	Repository repo;
	Service srv{ repo };
	srv.clearBoard();
	srv.setOnBoard(r, 1);
	srv.setOnBoard(n, 14);
	srv.setOnBoard(b, 36);
	srv.setOnBoard(q, 35);
	srv.setOnBoard(k, 2);
	srv.setOnBoard(pw, 10);
	srv.setOnBoard(pb, 51);
	auto rv = r->getSquares(srv.getBoard());
	vector<int>rvt = { 0,9,17,25,33,41,49,57 };

	auto nv = n->getSquares(srv.getBoard());
	vector<int>nvt = { 20,4,31,29 };
	auto bv = b->getSquares(srv.getBoard());
	vector<int>bvt = { 45,54,63,29,22,15,43,50,57,27,18,9,0 };
	auto qv = q->getSquares(srv.getBoard());
	vector<int>qvt = { 34,33,32,27,19,11,3,43,51,44,53,62,28,21,42,49,56,26,17,8 };
	auto kv = k->getSquares(srv.getBoard());
	vector<int>kvt = { 3,9,11 };
	auto pav = pw->getSquares(srv.getBoard());
	vector<int>pavt = { 18,26 };
	auto pbv = pb->getSquares(srv.getBoard());
	vector<int>pbvt = { 43 };
	assert(rv == rvt);
	assert(nv == nvt);
	assert(bv == bvt);
	assert(qv == qvt);
	assert(kv == kvt);
	assert(pav == pavt);
	assert(pbv == pbvt);
}

void Test::testCheck() {
	Repository r;
	Service srv{ r };
	ChessPiece* k = new King{ "White",1 };
	ChessPiece* rB = new Rook{ "Black",1 };
	ChessPiece* rW = new Rook{ "White",1 };
	ChessPiece* bB = new Bishop{ "Black",1 };
	ChessPiece* pB = new Pawn{ "Black",1 };
	ChessPiece* nB = new Knight{ "Black",1 };
	srv.clearBoard();
	srv.setOnBoard(k, 35);
	srv.setOnBoard(rB, 3);
	srv.setOnBoard(rW, 59);
	assert(srv.isInCheck(k) == true);
	srv.setOnBoard(rW, 11);
	assert(srv.isInCheck(k) == false);
	srv.setOnBoard(bB, 62);
	assert(srv.isInCheck(k) == true);
	srv.setOnBoard(rW, 53);
	srv.setOnBoard(rB, 2);
	assert(srv.isInCheck(k) == false);
	srv.setOnBoard(pB, 44);
	assert(srv.isInCheck(k) == true);
	srv.setOnBoard(pB, 45);
	assert(srv.isInCheck(k) == false);
	srv.setOnBoard(nB, 41);
	assert(srv.isInCheck(k) == true);
	srv.setOnBoard(rB, 41);
	assert(srv.isInCheck(k) == false);
	

}

void Test::testAll() {
	this->testDomain();
	this->testCheck();
}