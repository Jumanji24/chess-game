#include "SquareGUI.h"


void SquareGUI::paintEvent(QPaintEvent* ev) {
	QPainter painter(this);
	QPixmap pixmap1(QString::fromStdString(this->path));
	painter.drawPixmap(0, 0, this->width(), this->height(), pixmap1);
	if (this->square.getPiece() != nullptr) {
		if (this->square.getPiece()->getColor() == "White") {
			if (this->square.getPiece()->getType() == "Pawn") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/WhitePawn.png");
				this->xSize = 45;
				this->ySize = 55;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "Rook") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteRook.png");
				this->xSize = 50;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "Knight") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteKnight.png");
				this->xSize = 55;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "Bishop") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteBishop.png");
				this->xSize = 50;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "Queen") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteQueen.png");
				this->xSize = 65;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "King") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/WhiteKing.png");
				this->xSize = 65;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
		}
		else {
			if (this->square.getPiece()->getType() == "Pawn") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/BlackPawn.png");
				this->xSize = 45;
				this->ySize = 55;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "Rook") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/BlackRook.png");
				this->xSize = 50;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "Knight") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/BlackKnight.png");
				this->xSize = 55;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "Bishop") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/BlackBishop.png");
				this->xSize = 50;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "Queen") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/BlackQueen.png");
				this->xSize = 65;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
			if (this->square.getPiece()->getType() == "King") {
				this->pixPiece = new QPixmap("D:/Projects/ChessEngine/ChessEngine/PNG/BlackKing.png");
				this->xSize = 65;
				this->ySize = 65;
				painter.drawPixmap((this->width() - this->xSize) / 2, (this->height() - this->ySize) / 2, this->xSize, this->ySize, *this->pixPiece);
			}
		}
	}
}