#pragma once
#include <string>
#include <vector>

using namespace std;
class Square;

class ChessPiece {
private:
	string color;
	int value;
	int index;
	bool moved;
	int numOfMoves;//varaible used only for en passant
	int oldIdx;
protected:

	vector<double> squareValue;
public:
	ChessPiece() = default;
	ChessPiece(const string& color, const int& value, const int& index) :
		color{ color }, moved{ false }, value{ value },
		index{ index }, numOfMoves{ 0 }, oldIdx{ -1 } {}
	string getColor() const { return this->color; }
	int getValue() const { return this->value; }
	int getIndex()const { return this->index; }
	bool validateIndex(const int& idx) const { return idx >= 0 && idx <= 63; }
	virtual string getType() = 0;
	int moveUp(const int& idx);
	int moveRight(const int& idx);
	int moveLeft(const int& idx);
	int moveDown(const int& idx);
	int moveDiagLeftUp(const int& idx);
	int moveDiagRightUp(const int& idx);
	int moveDiagLeftDown(const int& idx);
	int moveDiagRightDown(const int& idx);
	int getOldIdx() const { return this->oldIdx; }
	void setOldIdx(const int& oldIdx) { this->oldIdx = oldIdx; }
	int getNumOfMoves() const { return this->numOfMoves; }
	void setNumOfMoves(int i) { this->numOfMoves = i; }
	vector<double>& getSqareValues() {
		return this->squareValue;
	}
	void incNumOfMoves() { this->numOfMoves++; }
	void setMoved() { this->moved = true; }
	bool hasMoved()const { return this->moved; }
	void setIndex(const int& idx) { this->index = idx; }
	virtual vector<int> getSquares(vector<Square>& tabla) = 0;
};

class Pawn : public ChessPiece {
private:
public:
	Pawn() = default;
	Pawn(const string& color, const int& idx) : ChessPiece(color, 100, idx) {
		if (color == "Black") {
			this->squareValue = vector<double>{ 0,  0,  0,  0,  0,  0,  0,  0,
												50, 50, 50, 50, 50, 50, 50, 50,
												10, 10, 20, 30, 30, 20, 10, 10,
												5,  5, 10, 27, 27, 10,  5,  5,
												0,  0,  0, 25, 25,  0,  0,  0,
												5, -5,-10,  0,  0,-10, -5,  5,
												5, 10, 10,-25,-25, 10, 10,  5,
												0,  0,  0,  0,  0,  0,  0,  0 };
		}
		else {
			this->squareValue = vector<double>{ 0,  0,  0,  0,  0,  0,  0,  0 ,
												5, 10, 10,-25,-25, 10, 10,  5,
												5, -5,-10,  0,  0,-10, -5,  5,
												0,  0,  0, 25, 25,  0,  0,  0,
												5,  5, 10, 27, 27, 10,  5,  5,
												10, 10, 20, 30, 30, 20, 10, 10,
												50, 50, 50, 50, 50, 50, 50, 50,
												0,  0,  0,  0,  0,  0,  0,  0, };
		}

	}
	string getType() override {
		return "Pawn";
	}
	virtual vector<int> getSquares(vector<Square>& tabla) override;
};

class Knight : public ChessPiece {
public:
	Knight() = default;
	Knight(const string& color, const int& idx) : ChessPiece(color, 320, idx) {
		this->squareValue = vector<double>{ -50,-40,-30,-30,-30,-30,-40,-50,
											-40,-20,  0,  5, 5,  0,-20,-40,
											-30,  0, 10, 15, 15, 10,  0,-30,
											-30,  0, 15, 20, 20, 15,  0,-30,
											-30,  5, 15, 20, 20, 15,  5,-30,
											-30,  5, 10, 15, 15, 10,  5,-30,
											-40,-20,  0,  0,  0,  0,-20,-40,
											-50,-40,-20,-30,-30,-20,-40,-50, };

	}
	string getType() override {
		return "Knight";
	}
	virtual vector<int> getSquares(vector<Square>& tabla) override;
};

class Bishop : public ChessPiece {
public:
	Bishop() = default;
	Bishop(const string& color, const int& idx) : ChessPiece(color, 325, idx) {
		this->squareValue = vector<double>{
											-20,-10,-40,-10,-10,-40,-10,-20,
											-10,  5,  0,  0,  0,  0,  5,-10,
											-10, 10, 10, 10, 10, 10, 10,-10,
											-10,  0, 10, 10, 10, 10,  0,-10,
											-10,  5,  5, 10, 10,  5,  5,-10,
											-10,  0,  5, 10, 10,  5,  0,-10,
											-10,  0,  0,  0,  0,  0,  0,-10,
											-20,-10,-10,-10,-10,-10,-10,-20, };

	}
	string getType() override {
		return "Bishop";
	}
	virtual vector<int> getSquares(vector<Square>& tabla) override;
};

class Rook : public ChessPiece {
public:
	Rook() = default;
	Rook(const string& color, const int& idx) : ChessPiece(color, 500, idx) {
		if (color == "White") {
			this->squareValue = vector<double>{ 0,0,0,5,5,0,0,0,
											  -5,0,0,0,0,0,0,-5,
											  -5,0,0,0,0,0,0,-5,
											  -5,0,0,0,0,0,0,-5,
											  -5,0,0,0,0,0,0,-5,
											  -5,0,0,0,0,0,0,-5,
											   5,10,10,10,10,10,10,5,
											   0,0,0,0,0,0,0,0 };
		}
		else
		{
			this->squareValue = vector<double>{ 0,0,0,5,5,0,0,0,
												 5,10,10,10,10,10,10,5,
												 -5,0,0,0,0,0,0,-5,
												 -5,0,0,0,0,0,0,-5,
												 -5,0,0,0,0,0,0,-5,
												 -5,0,0,0,0,0,0,-5,
												 -5,0,0,0,0,0,0,-5,
												  0,0,0,0,0,0,0,0 };

		}
	}
	string getType() override {
		return "Rook";
	}
	virtual vector<int> getSquares(vector<Square>& tabla) override;
};


class Queen : public ChessPiece {
public:
	Queen() = default;
	Queen(const string& color, const int& idx) : ChessPiece(color, 975, idx) {
		this->squareValue = vector<double>{ -20,-10,-10,-5,-5,-10,-10,-20,
											-10,0,5,0,0,0,0,-10,
											-10,5,5,5,5,5,0,-10,
											-5,0,5,5,5,5,0,-5,
											 0,0,5,5,5,5,0,-5,
											-10,0,5,5,5,5,0,-10,
											-10,0,0,0,0,0,0,-10,
											-20,-10,-10,-5,-5,-10,-10,-20 };
	}
	string getType() override {
		return "Queen";
	}
	virtual vector<int> getSquares(vector<Square>& tabla) override;
};


class King : public ChessPiece {
public:
	King() = default;
	King(const string& color, const int& idx) : ChessPiece(color, 32000, idx) {
		this->squareValue = vector<double>{
											20,  30,  10,   0,   0,  10,  30,  20,
											20,  20,   0,   0,   0,   0,  20,  20,
											-10, -20, -20, -20, -20, -20, -20, -10,
											-20, -30, -30, -40, -40, -30, -30, -20,
											-30, -40, -40, -50, -50, -40, -40, -30,
											-30, -40, -40, -50, -50, -40, -40, -30,
											-30, -40, -40, -50, -50, -40, -40, -30,
											-30, -40, -40, -50, -50, -40, -40, -30,
		};
	}
	string getType() override {
		return "King";
	}
	virtual vector<int> getSquares(vector<Square>& tabla) override;
};

class Square {
private:
	ChessPiece* piece;
public:
	Square() :piece{ nullptr } {}
	ChessPiece* getPiece() const { return this->piece; }
	void setPiece(ChessPiece* piece) {
		this->piece = piece;
	}
};

//util function to make a copy piece
ChessPiece* createPieceCopy(ChessPiece* piece);