#include "Repository.h"

void Repository::initBoard()
{
	for (int i = 0; i < 64; i++) {
		this->tabla.push_back(Square{});
	}
	tabla[0].setPiece(new Rook{ "White",0 });
	tabla[7].setPiece(new Rook{ "White",7 });
	tabla[1].setPiece(new Knight{ "White",1 });
	tabla[6].setPiece(new Knight{ "White" ,6 });
	tabla[2].setPiece(new Bishop{ "White" ,2 });
	tabla[5].setPiece(new Bishop{ "White" ,5 });
	tabla[3].setPiece(new Queen{ "White" ,3 });
	tabla[4].setPiece(new King{ "White" ,4 });
	this->whiteKing = tabla[4].getPiece();
	for (int i = 8; i < 16; i++) {
		tabla[i].setPiece(new Pawn{ "White" ,i });
	}
	tabla[63].setPiece(new Rook{ "Black" ,63 });
	tabla[56].setPiece(new Rook{ "Black" ,56 });
	tabla[62].setPiece(new Knight{ "Black" ,62 });
	tabla[57].setPiece(new Knight{ "Black" ,57 });
	tabla[61].setPiece(new Bishop{ "Black" ,61 });
	tabla[58].setPiece(new Bishop{ "Black" ,58 });
	tabla[60].setPiece(new King{ "Black" ,60 });
	this->blackKing = tabla[60].getPiece();
	tabla[59].setPiece(new Queen{ "Black" ,59 });

	for (int i = 48; i < 56; i++) {
		tabla[i].setPiece(new Pawn{ "Black" ,i });
	}
}

void Repository::initMapCoord() {
	this->mapCoord.insert(make_pair(0, "a"));
	this->mapCoord.insert(make_pair(1, "b"));
	this->mapCoord.insert(make_pair(2, "c"));
	this->mapCoord.insert(make_pair(3, "d"));
	this->mapCoord.insert(make_pair(4, "e"));
	this->mapCoord.insert(make_pair(5, "f"));
	this->mapCoord.insert(make_pair(6, "g"));
	this->mapCoord.insert(make_pair(7, "h"));
}

vector<Square>& Repository::getBoard() {
	return this->tabla;
}

bool Repository::updateBoard(ChessPiece* piece, const int& idx) {
	ChessPiece* p = this->tabla[idx].getPiece();
	auto v = piece->getSquares(this->tabla);
	bool ok = false;
	for (auto& el : v) {
		if (idx == el) {
			ok = true;
			break;
		}
	}
	if (ok) {
		auto curent_idx = piece->getIndex();
		if (curent_idx != idx) {
			//notations
			if (piece->getColor() == "White") {
				if (piece->getType() == "Pawn") {
					this->moves.push_back(make_pair(this->mapCoord[idx % 8] + to_string(idx / 8 + 1), ""));
				}
				else if (piece->getType() == "King" && !piece->hasMoved()) {
					//short castle
					if (idx - curent_idx == 2) {
						this->moves.push_back(make_pair("0-0", ""));
					}
					//long castle
					else if (idx - curent_idx == -2) {
						this->moves.push_back(make_pair("0-0-0", ""));
					}
					else {
						this->moves.push_back(make_pair(piece->getType()[0] + this->mapCoord[idx % 8] + to_string(idx / 8 + 1), ""));
					}
				}
				else if (piece->getType() == "Knight") {
					this->moves.push_back(make_pair("N" + this->mapCoord[idx % 8] + to_string(idx / 8 + 1), ""));
				}
				else {
					this->moves.push_back(make_pair(piece->getType()[0] + this->mapCoord[idx % 8] + to_string(idx / 8 + 1), ""));
				}
			}
			else {
				if (piece->getType() == "Pawn") {
					this->moves.back().second = this->mapCoord[idx % 8] + to_string(idx / 8 + 1);
				}
				else if (piece->getType() == "King" && !piece->hasMoved()) {
					//short castle
					if (idx - curent_idx == 2) {
						this->moves.back().second = "0-0";
					}
					//long castle
					else if (idx - curent_idx == -2) {
						this->moves.back().second = "0-0-0";
					}
					else {
						this->moves.back().second = piece->getType()[0] + this->mapCoord[idx % 8] + to_string(idx / 8 + 1);
					}
				}
				else if (piece->getType() == "Knight") {
					this->moves.back().second = "N" + this->mapCoord[idx % 8] + to_string(idx / 8 + 1);
				}
				else {
					this->moves.back().second = piece->getType()[0] + this->mapCoord[idx % 8] + to_string(idx / 8 + 1);
				}
			}
			//en passant
			if (piece->getType() == "Pawn") {
				if (piece->getColor() == "White") {
					if (idx == piece->moveDiagLeftUp(curent_idx)) {
						if (this->tabla[idx].getPiece() == nullptr) {
							this->tabla[idx].setPiece(piece);
							this->tabla[idx - 8].setPiece(nullptr);
							this->tabla[curent_idx].setPiece(nullptr);
							piece->setOldIdx(piece->getIndex());
							piece->setIndex(idx);
							return true;
						}
					}
					else if (idx == piece->moveDiagRightUp(curent_idx)) {
						if (this->tabla[idx].getPiece() == nullptr) {
							this->tabla[idx].setPiece(piece);
							this->tabla[idx - 8].setPiece(nullptr);
							this->tabla[curent_idx].setPiece(nullptr);
							piece->setOldIdx(piece->getIndex());
							piece->setIndex(idx);
							return true;
						}
					}
				}
				else {
					if (idx == piece->moveDiagLeftDown(curent_idx)) {
						if (this->tabla[idx].getPiece() == nullptr) {
							this->tabla[idx].setPiece(piece);
							this->tabla[idx + 8].setPiece(nullptr);
							this->tabla[curent_idx].setPiece(nullptr);
							piece->setOldIdx(piece->getIndex());
							piece->setIndex(idx);
							return true;
						}
					}
					else if (idx == piece->moveDiagRightDown(curent_idx)) {
						if (this->tabla[idx].getPiece() == nullptr) {
							this->tabla[idx].setPiece(piece);
							this->tabla[idx + 8].setPiece(nullptr);
							this->tabla[curent_idx].setPiece(nullptr);
							piece->setOldIdx(piece->getIndex());
							piece->setIndex(idx);
							return true;
						}
					}
				}
			}

			if (p != nullptr) {
				if (piece->getColor() != p->getColor()) {
					this->tabla[idx].setPiece(piece);
					this->tabla[curent_idx].setPiece(nullptr);
					piece->setOldIdx(piece->getIndex());
					piece->setIndex(idx);
					if (piece->getType() == "King") {
						if (piece->getColor() == "White") {
							this->whiteKing = piece;
						}
						else {
							this->blackKing = piece;
						}
					}
					delete p;
					return true;
				}
			}
			else {
				this->tabla[idx].setPiece(piece);
				piece->setOldIdx(piece->getIndex());
				piece->setIndex(idx);
				this->tabla[curent_idx].setPiece(nullptr);
				if (piece->getType() == "King") {
					if (piece->getColor() == "White") {
						this->whiteKing = piece;
					}
					else {
						this->blackKing = piece;
					}
				}
				return true;
			}
		}

	}
	return false;
}

void Repository::setBoard(vector<Square>& v) {
	this->tabla.clear();
	for (int i = 0; i < v.size(); i++) {
		this->tabla.push_back(Square{});
		if (v[i].getPiece() != nullptr) {
			tabla[i].setPiece(createPieceCopy(v[i].getPiece()));
		}
	}
}

void Repository::setNotation(vector<pair<string, string>>& moves) {
	this->moves.clear();
	this->moves = moves;
}

void Repository::clearBoard() {
	for (auto& el : this->tabla) {
		delete el.getPiece();
		el.setPiece(nullptr);
	}
}